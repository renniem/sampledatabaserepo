# Foreign Key Constraints
- [Home](../../../../TableOfContents.md)
- [Parent](../Standards.md)

-----

###	About
- Foreign keys must be created to enforce referential integrity.
- Foreign keys must be created for tables, if the design allows for it.
- All foreign key constraints should be created as a named constraint.
- i.e.  The name of the constraint must follow the naming convention laid out in this document and not be left to the SQL Server naming convention.  
- Foreign keys are scripted into a separate file per foreign key.

### Naming Convention
| Prefix | DB Naming Convention                                                               | Disk Naming Convention                                                                    |
|--------|------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| fk     | schema.fk\_TableName\_Column\_ReferencedTableName\_ReferencedColumnName            | schema.ObjectName.fk.sql                                                                  |
|        | e.g.  fk_tb_PTSNet_NotePlayer_NoteCagegoryID_tb_PTSNet_NoteCategory_NoteCategoryID | e.g.  fk_tb_PTSNet_NotePlayer_NoteCagegoryID_tb_PTSNet_NoteCategory_NoteCategoryID.fk.sql |
              
