# Unique Key Constraints
- [Home](../../../../TableOfContents.md)
- [Parent](../Standards.md)

-----

###	About
- All unique key constraints should be created as a named constraint.
- i.e.  The name of the constraint must follow the naming convention laid out in this document and not be left to the SQL Server naming convention.

#### Unique Index vs Unique Constraint
- Both of these enforce uniqueness in the data.
  - [See Unique Key vs Unique Constraint](http://stackoverflow.com/questions/10263760/unique-key-vs-unique-index-on-sql-server-2008 "Unique Key vs Unique Constraint")

### Naming Convention
| Prefix | DB Naming Convention                                      | Disk Naming Convention                                       |
|--------|-----------------------------------------------------------|--------------------------------------------------------------|
| uk     | schema.uk\_{TableName}\_{Columns}                         | schema.ObjectName.idx.sql                                    |
|        | e.g.  uk_tb_PTSNet_NoteCategory_NoteCategoryDescription   | e.g.  uk_PTSNet_NoteCategory_NoteCategoryDescription.idx.sql |
              
