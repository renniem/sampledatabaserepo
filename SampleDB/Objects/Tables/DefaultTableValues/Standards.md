# Default Table Values
- [Home](../../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About
- This will store presets for lookup or known data.

### Naming Convention
| Prefix | DB Naming Convention                     | Disk Naming Convention                            |
|--------|------------------------------------------|---------------------------------------------------|
| N/A    | N/A                                      | schema.ObjectName.PRESET.sql                      |
|        | N/A                                      | e.g.  dbo.tb_PTSNet_ApprovalRuleStatus.PRESET.sql |
            
