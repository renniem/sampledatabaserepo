# Tables
- [Home](../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About

- Tables must have a clear naming convention.
- These are some core requirements that needed to be fulfilled:
- Need to easily navigate/ group related tables
- Be able to programmatically interrogate table names for automation purposes.

### Table Rules
- Below are a list of general rules to keep in mind when designing tables.

#### Identity Columns
- The use of identity columns is allowed, but this column must never be used in any application specific code.
- The reason for this is to facilitate the merging and splitting of the database.
- When in doubt, ask yourself the question, �Can I reseed this column at any point without there being an impact to my application?�  If the answer is �Yes�, then identity column usage is fair game.  If the answer is �No, Not Really� (or any variation thereof), then identity columns should not be used.
- This column should be called **_ID**, unless there is very good reason not to.
- This should be discussed with the DB Team and documented.

#### Primary Keys
- All tables should have a primary key, unless there is a very good reason to create a heap.
  - This is usually discussed and agreed upon by the DB Team.
  - Exceptions should be documented to facilitate static code analysis.
- Where possible, add a single column primary key to a table, although multi-column keys are fine.
  - The usage of GUID�s are acceptable.
  - The usage of binary concatenated fields are fine

#### Indexes
- Indexes are created via the Index Maintenance Framework.  This is the preferred way.
- Adhoc indexes are allowed to be created in production by the DBA�s and these need to be evaluated to see if they need to be included in the standard list of indexes.

#### Constraints
- Constraints must be created where necessary.  More often than not it will be used to enforce a business rule.  E.g.  tb_Country cannot have duplicate �CountryName�.  

    - [Unique Key Constraints](Constraints/UniqueKeyConstraints/Standards.md)
    - [Foreign Key Constraints](Constraints/ForeignKeyConstraints/Standards.md)  
    
    - #####	Default Constraints
      - All default constraints should be created as a named constraint.
        - i.e.  The name of the constraint must follow the naming convention laid out in this document and not be left to the SQL Server naming convention
      - Default constraints are created as part of the table create script.

        - ###### Naming Convention  
          
              schema.df_TableName_Column_DefaultValue
            
              e.g.  df_tb_PTSNet_NotePlayer_UTCDateCreated_SYSUTCDATETIME

    - #####	Check Constraints
      - All check constraints should be created as a named constraint.
        - i.e.  The name of the constraint must follow the naming convention laid out in this document and not be left to the SQL Server naming convention
      - Check constraints are created as part of the table create script.
        
      - ###### Naming Convention  
          
              schema.ch_TableName_Column
            
              e.g.  ch_tb_PTSNet_NotePlayer_SourceSystemID

### Naming Convention
| Prefix | DB Naming Convention                  | Disk Naming Convention                  |
|--------|---------------------------------------|-----------------------------------------|
| tb     | schema.tb\_{DBType}\_{Grouping}\_Name | schema.ObjectName.TAB.sql               |
|        | e.g.  dbo.tb_PTSNet_Note_Categories   | e.g.  tb_PTSNet_Note_Categories.TAB.sql |
          
