# Triggers
- [Home](../../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About
- Use only where it make sense and with due consideration to the impact that it will have.
- Consult DB Team during the design phase.

### Naming Convention
| Prefix | DB Naming Convention                     | Disk Naming Convention                          |
|--------|------------------------------------------|-------------------------------------------------|
| tr     | schema.tr\_TableName\_Action             | schema.ObjectName.TRG.sql                       |
|        | e.g.  dbo.tr_PTSNet_PlayerDetail_Update  | e.g.  dbo.tr_PTSNet_PlayerDetail_Update.TRG.sql |
            
