# Assemblies
- [Home](../../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About
- Managed database objects, such as stored procedures or triggers, are compiled and then deployed in units called an assembly.
- Managed DLL assemblies must be registered in Microsoft SQL Server before the functionality the assembly provides can be used.
            
