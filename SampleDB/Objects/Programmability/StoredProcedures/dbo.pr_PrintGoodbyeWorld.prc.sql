IF OBJECT_ID('dbo.pr_PrintGoodbyeWorld') IS NULL
BEGIN
	EXEC ('CREATE PROCEDURE dbo.pr_PrintGoodbyeWorld AS SET NOCOUNT ON;')
END
GO

-- DROP PROC dbo.[pr_PrintGoodbyeWorld]
ALTER PROCEDURE dbo.pr_PrintGoodbyeWorld

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  PRINT "Hello World"
END
GO