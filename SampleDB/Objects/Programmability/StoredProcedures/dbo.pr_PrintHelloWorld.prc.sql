IF OBJECT_ID('dbo.pr_PrintHelloWorld') IS NULL
BEGIN
	EXEC ('CREATE PROCEDURE dbo.pr_PrintHelloWorld AS SET NOCOUNT ON;')
END
GO

-- DROP PROC dbo.[pr_PrintHelloWorld]
ALTER PROCEDURE dbo.pr_PrintHelloWorld

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  PRINT "Hello World"
END
GO