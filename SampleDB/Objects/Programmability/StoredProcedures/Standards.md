# Stored Procedures
- [Home](../../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About
- Belong to the dbo schema by default.
- Are accessed via a synonym when called from the middle-tier.
- Follows a verbose naming convention to help group objects together

### Naming Convention
| Prefix | DB Naming Convention                     | Disk Naming Convention                                |
|--------|------------------------------------------|-------------------------------------------------------|
| pr     | schema.pr\_{Grouping}\_ObjectName        | Schema.ObjectName.PRC.sql                             |
|        | dbo.pr_PTSNet_LB_UpdateVaultStatus       | dbo.pr_PTSNet_LB_UpdateVaultLockStatus.PRC.sql        |
          
