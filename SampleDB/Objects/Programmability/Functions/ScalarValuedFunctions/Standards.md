# Scalar Valued Functions
- [Home](../../../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About
- Use only where it makes sense.
- WITH SCHEMABINDING
  - If your function does not touch any data containers, i.e. tables or views then the [WITH SCHEMABINDING] keyword must be used.

### Naming Convention
| Prefix | DB Naming Convention                     | Disk Naming Convention                                |
|--------|------------------------------------------|-------------------------------------------------------|
| fn     | schema.fn\_{Grouping}\_ObjectName        | Schema.ObjectName.UDF.sql                             |
|        | dbo.fn_PTSNet_BTM_GetConstraintFromXML   | dbo.fn_PTSNet_BTM_GetConstraintFromXML.UDF.sql        |
              
