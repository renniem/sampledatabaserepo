# Schemas
- [Home](../../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About
- The use of schemas are limited to physical process isolation and not for business groupings.
- The reason for not using it to group business processes are that we tie a dynamic element (business process) to a static element (db schema).  As and when the dynamic element changes, the static element becomes less meaningful and more confusing.
- Accepted usage of schemas is for physical process isolation. 

### Naming Convention
| Prefix | DB Naming Convention         | Disk Naming Convention  |
|--------|------------------------------|-------------------------|
| N/A    | N/A                          | ObjectName.schema.sql   |
|        | e.g.  PTSNet                 | e.g.  PTSNet.TAB.sql    |
            
