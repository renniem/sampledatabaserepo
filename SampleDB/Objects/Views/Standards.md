# Views
- [Home](../../TableOfContents.md)
- [Parent](../Standards.md)

----------

###	About
- Use only where it makes sense.  i.e. it must add value

### Naming Convention
| Prefix | DB Naming Convention                     | Disk Naming Convention                            |
|--------|------------------------------------------|---------------------------------------------------|
| vw     | schema.vw\_ObjectName                    | Schema.ObjectName.VIW.sql                         |
|        | dbo.vw_PTSNet_InstalledCasino            | dbo.vw\_PTSNet\_InstalledCasino.VIW.sql           |
          
