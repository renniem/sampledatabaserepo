# General Database Rules 
----------
### DirectoryStructure
- DirectoryStructure is a script that was used to produce this folder hierarchy along with the standards.md documents.
- If there are changes or modifications required either to the folder hierarchy of the standards, please chat to Rennie Moodley.

----------

###	Preferred Software 
 
- SQL Server Management Studio is the preferred software for database development, however, a developer can use any software that they are comfortable with, provided it adheres to the standards laid out in this document. 
  - See "Development Environment (IDE)" below. 
 
###	Source Control 
 
- All database code must be checked into the agreed upon source control. 
- All database code must ensure that it conforms to the agreed up best practices. 
 
###	Releases 
 
- All database code must be released via the configured release mechanism. 
- Releases will follow this cycle 
  - Dev -> QA -> Regression -> Signoff -> Production. 
 
###	Dev Testing 
 
- All code must be tested independently to ensure that you have fixed the correct issue without introducing another. 
- All code must be tested as part of an install to ensure that you have not affected the release/ install process 
 
###	Peer Reviews 
 
- Peer reviews are a vital part of learning.  As such, all database code should be peer reviewed. 
- At the very least, all cases linked to database changes will have a peer review status set. 
  - If set to "no peer review required", the developer will hold absolute responsibility for any issues regarding that code. 
- QA are the gatekeepers of this process and will not accept a case for testing unless it has a peer review status set. 
- Peer reviews along with static code analysis tools will be used to catch issues as close to the development phase as is possible.  Issues caught past the development phase need to be analyzed to see why it was not picked up earlier and potentially create an automated method of checking this or highlighting it as a peer review check going forward. 
 
###	Only Alter Objects 
 
- Where possible, all objects should be defensively coded to create a stub of an object if it does not exist and alter the object if it does exist. 
 
###	DB Object Composition 
 
- Should have the following sections: 
  - Comment Block 
    - Brief description of what the object does. 
  - Change Log 
    - Detailing changes that were made 
      - Who 
      - Version 
      - When 
      - Description 
  - The description must contain a case as this will help with troubleshooting. 
    - Completing the change log is NOT an optional step. 
  - Defensive Create Stub (where applicable) 
    - Object should be defensively created with a stub of the object. 
  - Alter Object 
    - Once the stub is created, it should only be altered thereafter. 
 
###	Database Time Settings 
 
- As a general rule, all database times that are under our control, should use SYSUTCDATETIME() 
- Exceptions to this rule must be discussed with the DB Team and documented.
    
