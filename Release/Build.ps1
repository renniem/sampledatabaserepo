﻿cls

$WorkingDirectory =  $PSScriptRoot #"D:\!!!TC\!!!SourceControl\repos\sampledatabaserepo"

$FilesToInclude = Get-ChildItem -Path $WorkingDirectory -Recurse -Filter "*.sql"

[string] $PatchContents = ""
[string] $CRLF = "`r`n"

foreach ($File in $FilesToInclude)
{
    $ObjectName = $File.Name

    $PatchContents += "PRINT GETDATE()" + $CRLF
    $PatchContents += "GO" + $CRLF
    $PatchContents += $CRLF

    $PatchContents += "PRINT 'Executing $ObjectName'" + $CRLF
    $PatchContents += "GO" + $CRLF
    $PatchContents += $CRLF
    
    $ObjectContents = ((Get-Content -Path ($File.FullName)) -join "`r`n")
    $PatchContents += $ObjectContents  + $CRLF
    $PatchContents += "GO" + $CRLF
    $PatchContents += $CRLF
}

$BuildDirectory = $WorkingDirectory + "\Build"

if (Test-Path -Path $BuildDirectory)
{
  # recursively delete files in $Directory
  Get-ChildItem -Path "$BuildDirectory\\*" -Recurse | Remove-Item -Force -Recurse
       
  # now remove the directory
  Remove-Item -Path $BuildDirectory -Recurse
}
else
{
  New-Item -Path $BuildDirectory -ItemType directory | Out-Null
}

Set-Content -Path ("$BuildDirectory\SQLPatch.sql") -Value $PatchContents
