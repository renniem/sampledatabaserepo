# Jobs
- [Home](../TableOfContents.md)
  - [Parent](../Standards.md)

----------

###	About
  - Jobs should be used sparingly as there is a huge knock on effect in production.
  - Where possible, jobs should be self-deleting.
  - Jobs should be created from a stored procedure and even if the job is deleted, it can be recreated by executing the stored procedure.

### Naming Convention
| Prefix | DB Naming Convention                                           | Disk Naming Convention                              |
|--------|----------------------------------------------------------------|-----------------------------------------------------|
| N/A    | {Product}_ObjectName_{DB_NAME}                                 | ObjectName.schema.sql                               |
|        | e.g.  Caiman_EventProcessing_Agent_PTSNet_SouthSeasGaming      | e.g.  CreateJob_CAIMAN_EventProcessing_Agent.sql    |
          
